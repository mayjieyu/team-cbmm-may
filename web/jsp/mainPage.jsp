<%--
  Created by IntelliJ IDEA.
  User: may
  Date: 22/05/2018
  Time: 3:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--<link rel="stylesheet" href="css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        body {
            background-color: red;
        }
        .container {
            width: 90%;
        }

        /* Nav start */
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
            background-color: white;
            color: black;
            font-size: 1.3em;
            border: 0;
        }

        .navbar-brand {
            float: left;
            min-height: 60px;
            padding: 14px 60px 15px;
        }

        .navbar-inverse .navbar-nav .active a,
        .navbar-inverse .navbar-nav .active a:focus,
        .navbar-inverse .navbar-nav .active a:hover
        {
            margin-bottom: 3px;
            border: 3px solid black;
            background-color: white;
        }

        .navbar-collapse {
            padding-right: 30px;
        }

        .navbar-nav>li>a {
            padding-top: 15px;
            padding-bottom: 15px;
            margin: 5px;
            color: red;
        }

        .navbar-toggle {
            position: relative;
            float: right;
            padding: 9px 10px;
            margin-top: 12px;
            margin-right: 15px;
            margin-bottom: 8px;
        }

        @media screen and (max-width:600px) {
            .navbar-brand {
                padding: 14px 35px 15px
            }
        }

        /* Nav end */

        .articleCard {
            background-color: white;
            height: 300px;
            padding: 16px;
            border-radius: calc(0.5rem - 1px);
        }
    </style>
</head>
<body>

<header>
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#"></a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#">My blog</a></li>
            </ul>
        </div>
    </nav>
    <br>
    <br>
        <%--<%@ include file="navbar.jsp" %>--%>
</header>

<%--todo: test if navbar can link to where it should go--%>
<%--but for now, I use a hyperlink for testing--%>
<%--go to homepage--%>
<a href="./HomePage">HomePage</a>

<div class="container container-margin">
<c:choose>
    <c:when test="${fn:length(articles) gt 0}">

        <c:forEach var="article" items="${articles}">

            <%--<div class="panel panel-default">--%>

            <%--<h3 class="panel-title pull-left"><a href="?article=${article.id}"></a></h3>--%>

            <div class="card" style="width: 18rem;">
                <img class="card-img-top" src="..." alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">${article.title}</h5>
                    <p class="card-text">${article.content}</p>
                    <a href="#" class="btn btn-primary">Load Full Article</a>
                </div>
            </div>


            <%--</div>--%>
            <%--<div class="panel-body">--%>
            <%--<p>${article.content}</p>--%>
            <%--</div>--%>
            </div>
        </c:forEach>
    </c:when>
    <c:otherwise>
        <p>No articles!</p>
    </c:otherwise>
</c:choose>
</div>
<br>
<div class="container">
    <form action="/Article" method="get">
        <button type="submit" class="btn btn-primary my-2">Article</button>
    </form>
</div>
<script type="text/javascript" src="../js/jquery.min.js"></script>
<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
