<%--
  Description: a home page for each user
  ----------------------------------------------------------
  Version  |   Date        |   Created by          |   Description
  v1       |   22/05/2018  |   Chinchien & Massie  |
--%>

<%@ page contentType="text/html;charset=UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>HomePage</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <%--<link href="${pageContext.request.contextPath}/css/page.css" rel="stylesheet" type="text/css"/>--%>
    <link rel="stylesheet" type="text/css" href="<c:url value='../css/page.css' />"/>
    <%--<link href="<c:url value="../css/page.css" />" rel="stylesheet">--%>
    <%--<jsp:include page="page.css"/>--%>

    <%--<script>--%>
        <%--function getFullContent(articleId) {--%>
            <%--//ceate a cookie and go to a full text servlet--%>
            <%--document.cookie = "article=" + articleId + "\"";--%>
            <%--// window.location="";--%>
            <%--document.alert("test");--%>
        <%--}--%>
    <%--</script>--%>

</head>
<body>
<header>
    <%@ include file="navbar.jsp" %>
</header>

<div class="container">
    <div class="row">
        <div class="col-md-4 informationColumn">
            <div class="col-md-12 contentCard">
                <img id="avatar" src="../image/winkMan.png" style="width: 200px" alt="">
                <div class="userInfor" >
                    <div class="userInforRow">
                        <p class="userInforTitle">UserName :</p><p class="userInforDetail">Tom</p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Email :</p><p class="userInforDetail">11111111@Aucklanduni</p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Birthday :</p><p class="userInforDetail">XXXX</p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Country :</p><p class="userInforDetail">NZ</p>
                    </div>
                    <div class="userInforRow">
                        <p class="userInforTitle">Description :</p><p class="userInforDetail" style="text-align: justify">Hundreds of millions of years ago, a group of extraordinary creatures dominated the planets: dinosaurs.</p>
                    </div>
                    <br>
                    <br>
                    <div class="userInforRow">
                        <p class="userInforTitle" style="float: right; color: #2aabd2;">Edit</p>
                    </div>


                </div>
            </div>
        </div>
        <div class="col-md-8 articleColumn">
            <div class="col-md-12 contentCard02">
                <h3 class="headerType">asdfgghd</h3>
                <hr class="line">
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>

            </div>
            <div class="col-md-12 contentCard02">
                <h3 class="headerType">asdfgghd</h3>
                <hr class="line">
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
            </div>

            <div class="col-md-12 contentCard02">
                <h3 class="headerType">asdfgghd</h3>
                <hr class="line">
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
            </div>
            <div class="col-md-12 contentCard02">
                <h3 class="headerType">asdfgghd</h3>
                <hr class="line">
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
                <p>sfdsggdhsdgdhfksjfk</p>
            </div>
        </div>
    </div>

</div>

<%--<div class="container">--%>
    <%--<div class="row">--%>
        <%--test--%>
        <%--&lt;%&ndash;user info&ndash;%&gt;--%>
        <%--<div class="col-3">--%>
            <%--<img src="${user.avatar}">--%>
            <%--<br>--%>
            <%--${user.username}--%>
            <%--<br>--%>
            <%--${user.description}--%>

        <%--</div>--%>
        <%--&lt;%&ndash;articles&ndash;%&gt;--%>
        <%--<div class="col-9">--%>
            <%--test--%>
            <%--<c:forEach var="article" items="${articles}">--%>
                <%--<section class="article">--%>
                    <%--<p>--%>
                            <%--${article.title}--%>
                        <%--<br>--%>
                            <%--${article.content}--%>
                        <%--<br>--%>
                        <%--<button onclick="getFullContent(${article.id})">read more</button>--%>
                    <%--</p>--%>
                <%--</section>--%>
            <%--</c:forEach>--%>
        <%--</div>--%>
    <%--</div>--%>

<%--</div>--%>

<%--</body>--%>
<%--<script type="text/javascript" src="js/jquery.min.js"></script>--%>
<%--<script src="http://cdn.static.runoob.com/libs/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
</html>
