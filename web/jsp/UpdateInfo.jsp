<%--
  Created by IntelliJ IDEA.
  User: Chiron
  Date: 23/05/18
  Time: 11:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: black;
        }

        * {
            box-sizing: border-box;
        }

        /* Add padding to containers */
        .container {
            padding: 20px;
            background-color: white;
        }

        /* Full-width input fields */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            display: inline-block;
            border: none;
            background: #f1f1f1;
        }

        input[type=text]:focus, input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Overwrite default styles of hr */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* Set a style for the submit button */
        .registerbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px 20px;
            margin: 8px 0;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
        }

        .registerbtn:hover {
            opacity: 1;
        }

        /* Add a blue text color to links */
        a {
            color: dodgerblue;
        }

        /* Set a grey background color and center the text of the "sign in" section */
        .signin {
            background-color: #f1f1f1;
            text-align: center;
        }
    </style>
</head>
<body>
<!--todo complete action for form and method-->
<form action="/Registration" Method="POST">
    <div class="container">
        <h1>Register</h1>
        <p>Please update your details here.</p>
        <hr>

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Enter Email" name="email" id="email" value="${user.email}">
        <label for="fname"><b>First Name</b></label>
        <input type="text" placeholder="Your first name here" name="fname" id="fname" value="${user.fname}" required>

        <label for="lname"><b>Last Name</b></label>
        <input type="text" placeholder="Your last name here" name="lname" id="lname" value="${user.lname}"  required>

        <label for="userName"><b>User Name</b></label>
        <input type="text" placeholder="Your user name here" name="username" id="userName" value="${user.username}"  required>

        <label for="psw"><b>Password</b></label>
        <input type="password" placeholder="Enter Password" name="hashed_code" id="psw" required>

        <label for="psw-repeat"><b>Repeat Password</b></label>
        <input type="password" placeholder="Repeat Password" name="hashed_code" id="psw-repeat"  required>

        <label for="dob"><b>Date of Birth</b></label>
        <div>
            <input type="date" name="dob" id="dob" value="${user.birthday}"  required>
        </div>
        <br>
        <%@ include file="countryselection.jsp" %>


        <br>
        <div>
            <label for="userDescr"><b>Description</b></label>
            <div id="userDescr" name="descrp" >
            <textarea rows="5" cols="50" placeholder="${user.descrp}">


            </textarea>
            </div>
        </div>

        <hr>
        <p>By creating an account you agree to our <a href="#">Terms & Privacy</a>.</p>

        <button type="submit" class="registerbtn" style="font-size:120%;">Register</button>
    </div>

    <div class="container signin">
        <p>Already have an account? <a href="#">Sign in</a>.</p>
    </div>
</form>

</body>
</html>
