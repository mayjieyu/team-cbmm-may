package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.User;
import DAOs.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class HomePageServlet extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //todo from main to home page
        //we can know where the request came from by adding "?source=main" to the url
//        String source =  req.getParameter("source");
        HttpSession sess = req.getSession(true);
        String username = (String) sess.getAttribute("username");
        User userInfo = null;
        List<Article> articles = null;
        if (username != null){
            //get all user info
            try(UserDAO userDAO = new UserDAO()){
                userInfo = userDAO.getAllUserInfoByName(username);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //get user's articles
            try(ArticleDAO articleDAO = new ArticleDAO()){
                articles = articleDAO.getArticlesByName(username);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        req.setAttribute("userInfo", userInfo);
        req.setAttribute("articles", articles);

        req.getRequestDispatcher("jsp/homePage.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);

    }
}
