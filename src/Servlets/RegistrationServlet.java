package Servlets;

//import org.apache.commons.fileupload.FileUploadException;
//import org.apache.commons.fileupload.disk.DiskFileItemFactory;
//import org.apache.commons.fileupload.servlet.ServletFileUpload;
//import org.apache.commons.fileupload.FileItem;

import DAOs.User;
import DAOs.UserDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class RegistrationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//

     createNewUser(req);

            resp.sendRedirect("SOME PATH HERE");



    }

    public void uploadImage(HttpServletRequest req) {

        Image toStore = null;


        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        ServletFileUpload upload = new ServletFileUpload(factory);

        try {

            List<FileItem> fileItems = upload.parseRequest(req);

            File fullsizeImageFile = null;

            // Extract / download file and caption
            for (FileItem fi : fileItems) {


                // The file
                if (!fi.isFormField()) {
                    String fileName = fi.getName();
                    fullsizeImageFile = new File(fileName);
                    toStore = thumbnailMaker(fullsizeImageFile);


                }
            }

        } catch (FileUploadException e) {
            e.getMessage();

        }
    }


    public void createNewUser(HttpServletRequest req){
        try {
            User newUser=new User();


            newUser.setFname(req.getParameter("fname"));
            newUser.setLname(req.getParameter("lname"));
            newUser.setRole(req.getParameter("role"));
            newUser.setUsername(req.getParameter("userName"));
            newUser.setBirthday(req.getParameter("doB"));
            newUser.setCountry(req.getParameter("country"));
            newUser.setDescrp(req.getParameter("description"));

            UserDAO makeUser=new UserDAO();
            makeUser.createNewUser(newUser);

        }catch (SQLException|IOException e){
            e.getMessage();
        }
    }

    //code for making thumbnail;

    public Image thumbnailMaker(File fullImage) {
        Image img = null;
        try {
            img = ImageIO.read(fullImage).getScaledInstance(100, 100, BufferedImage.SCALE_SMOOTH);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return img;
    }

//    public static byte[] convertFileContentToBlob(Image file) throws IOException {
//        // create file object
//
//        // initialize a byte array of size of the file
//        byte[] fileContent = new byte[(int) file.length()];
//
//        FileInputStream inputStream = null;
//        try {
//            // create an input stream pointing to the file
//            inputStream = new FileInputStream(file);
//            // read the contents of file into byte array
//            inputStream.read(fileContent);
//        } catch (IOException e) {
//            throw new IOException("Unable to convert file to byte array. " + e.getMessage());
//        } finally {
//            // close input stream
//            if (inputStream != null) {
//                inputStream.close();
//            }
//        }
//        return fileContent;
//
//    }
}
