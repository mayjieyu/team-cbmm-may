package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public class ArticleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        HttpSession myUser= req.getSession();
        displayArticlesList(req, resp);


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


       makeNewArticle(req,resp);



    }



    private void makeNewArticle(HttpServletRequest req, HttpServletResponse resp){

        try {
            Article article=new Article();


            int articleIdTest = Integer.parseInt(req.getParameter("articleId"));
            article.setArticleId(articleIdTest);
            article.setTitle(req.getParameter("title"));
            article.setContent(req.getParameter("content"));
            article.setStatus(req.getParameter("status"));
            article.setTopic(req.getParameter("topic"));
            article.setModifiedDateAndTime(req.getParameter("modifiedDateAndTime"));

            ArticleDAO makeNewArticle=new ArticleDAO();
            makeNewArticle.createNewArticle(article);
        }catch (SQLException|IOException e){
            e.getMessage();
        }
    }

    private void displayArticlesList(HttpServletRequest request, HttpServletResponse response) {


        try (ArticleDAO dao = new ArticleDAO()) {

            List<Article> articles = dao.getAllArticles();

            // Adding the article list to the request object
            request.setAttribute("articles", articles);

            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);


        }catch (Exception e){
            e.getMessage();
        }
    }

    private void displaySingleArticle(HttpServletRequest request, HttpServletResponse response, String name){

        try (DAOs.ArticleDAO dao = new ArticleDAO()) {

            List<Article> article = dao.getArticlesByName(name);
            if (article == null) {
                response.sendError(404);
                return;
            }

            // Adding the article to the request object so that our article.jsp page can access it
            request.setAttribute("Article", article);

            // Redirect to /WEB-INF/example03/article_jstl.jsp, which will now have access to the article through its
            // request object, due to the line above.
            request.getRequestDispatcher("mainPage.jsp").forward(request, response);

        }catch (Exception e){
            e.getMessage();
        }
    }


private void deleteArticle(HttpServletRequest request, HttpServletResponse response, String name){
        
        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database
        
    try {
        ArticleDAO toControl=new ArticleDAO();
        toControl.deleteArticle(name);


        //redirects to main page
        request.getRequestDispatcher("articles.jsp").forward(request, response);


        // TODO: 23/05/18 tidyup this try/catch block 
    } catch (SQLException|IOException|ServletException e) {
        e.printStackTrace();
    }
  

}



}
