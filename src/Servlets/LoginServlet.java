package Servlets;
/**
 * Description: Servlets.LoginServlet servlet:
 * - login checking
 * ----------------------------------------------------------
 * Version  |   Date        |   Created by          |   Description
 * v1       |   21/05/2018  |   Chinchien           | username & pw checking, create a session attribute "username"
 * v2       |   22/05/2018  |   Chinchien & Massie  | get & set user's info & articles
 * v3       |   24/05/2018  |   Chinchien           | We change the design, after logging in, should direct user to the main page, so need to move v2 into another servlet
 * | Due to some connection errors to the DB, I change some codes on this one, DAO, and login html file
 */

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.User;
import DAOs.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class LoginServlet extends HttpServlet {
    //if we get any error msg, set session's attribute "errorMsg" to the message that we want to show up
    //when back to the login page
    private String errorMsg;
    Cookie cookie = new Cookie("errorMsg", ""); //clean the cookie "errorMsg"

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String inputPassword = request.getParameter("password");
        String password = null;
        //connect to the DB
        try (UserDAO userdao = new UserDAO()) {
            //check PW
            password = userdao.getPassword(username);

            //check if the user exits
            if (password == null) {
                //direct back to the login page, and store an error message in cookie
                //PS: a cookie can only store 32 chars
//                errorMsg = "incorrect username";
                errorMsg = "1";
                cookie = new Cookie("errorMsg", errorMsg);
                response.addCookie(cookie);
                response.sendRedirect("/login.html");
//                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
//                dispatcher.forward(request, response);
            }
            //check if the password exits: the input pw need to match the hashed pw
            if (password != null) {
                //todo pw security
//                byte[] bytecode = Passwords.base64Decode(password);
//                boolean isMatched = Passwords.isInsecureHashMatch(inputPassword, bytecode);
                boolean isMatched = password.equals(inputPassword);
                if (!isMatched) {
//                    errorMsg = "incorrect password";
                    errorMsg = "2";
                    cookie = new Cookie("errorMsg", errorMsg);
                    response.addCookie(cookie);
                    response.sendRedirect("/login.html");
//                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
//                    dispatcher.forward(request, response);
                } else {
                    request.getSession().setAttribute("username", username);
//                /** v2: before we go the the home page, we may need to get the user's info & articles */
//                User user = userdao.getAllUserInfoByName(username);
//                List<Article> articles = getArticlesByName(username);
//                request.getSession().setAttribute("user", user);
//                request.setAttribute("user", user);
//                request.setAttribute("articles", articles);
                    //direct to the main page
                    //todo
                    response.sendRedirect("Articles");
                }
            }
        } catch (SQLException e) {
//            errorMsg = "Our system is not available now";
            errorMsg = "3";
            cookie = new Cookie("errorMsg", errorMsg);
            response.addCookie(cookie);
            response.sendRedirect("/login.html");
//            request.setAttribute("errorMsg", errorMsg);
//            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
//            dispatcher.forward(request, response);
        } catch (Exception e) {
//            errorMsg = "Our system is not available now";
            errorMsg = "3";
            cookie = new Cookie("errorMsg", errorMsg);
            response.addCookie(cookie);
            response.sendRedirect("/login.html");
//            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.html");
//            dispatcher.forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * v2
     */
    protected List<Article> getArticlesByName(String username) {
        List<Article> articles = null;
        try (ArticleDAO articleDAO = new ArticleDAO()) {
            articles = articleDAO.getArticlesByName(username);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return articles;
    }

}
