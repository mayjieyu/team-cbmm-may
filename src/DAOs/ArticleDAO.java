package DAOs;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ArticleDAO implements AutoCloseable {
    private final Connection conn;


    public ArticleDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods
    public List<Article> getAllArticles() throws SQLException {
        List<Article> articles = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_articles")) {
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt("article_id"));
                    article.setTitle(r.getString("title"));
                    article.setContent(r.getString("content"));
                    article.setModifiedDateAndTime(r.getString("modified_date"));
                    article.setStatus((r.getString("status")));
                    article.setUsername(r.getString("username"));

                    articles.add(article);
                    return articles;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    public List<Article> getArticlesByName(String name) throws SQLException {
        List<Article> articles = new ArrayList<>();
//        Article article = new Article();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM articles WHERE title LIKE ?")) {

            stmt.setString(1, name);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    Article article = new Article();
                    article.setArticleId(r.getInt(1));
                    article.setTitle(r.getString(2));
                    article.setContent(r.getString(3));
                    article.setStatus(r.getString(4));
                    article.setModifiedDateAndTime(r.getString(5));
                    articles.add(article);
                }

                return articles;

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return articles;
    }


    public void createNewArticle(Article article){
        try {

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_articles VALUES (?,?,?,?,?,?,?,?)");
            {
                stmt.setInt(1, article.getArticleId());
                stmt.setString(2, article.getTitle());
                stmt.setString(3, article.getContent());
                stmt.setString(4, article.getStatus());
                stmt.setString(5, article.getTopic());
                stmt.setString(6, article.getModifiedDateAndTime());

//                    stmt.setBlob(8, );
            }

        } catch (SQLException e) {
            e.getMessage();
        }
    }


    public  void deleteArticle(String name) throws SQLException{

        try {

            PreparedStatement stmt = conn.prepareStatement("DELETE FROM Team_CBMM.blog_articles WHERE title LIKE ?");
            {
               stmt.setString(1,name);

            }

        } catch (SQLException e) {
            e.getMessage();
        }


    }




    @Override
    public void close() throws Exception {
        this.conn.close();
    }
}
