package DAOs;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements AutoCloseable {
    private final Connection conn;


    public UserDAO() throws IOException, SQLException {
        this.conn = HikariConnectionPool.getConnection();
    }

    //todo get what we want from the DB by different methods

    public User getAllUserInfoByName(String username) throws SQLException {
        User user = new User();
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM blog_users WHERE username = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                while (r.next()) {
                    user.setUserId(r.getInt(1));
                    user.setFname(r.getString(2));
                    user.setLname(r.getString(3));
                    user.setUsername(r.getString(4));
                    user.setBirthday(r.getString(5));
                    user.setCountry(r.getString(6));
                    user.setAvatar(r.getString(7));
                    user.setDescrp(r.getString(8));
                    user.setFname(r.getString(9));
                }
                return user;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return user;
    }

    public String getPassword(String username) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("SELECT DISTINCT password " +
                "FROM blog_users " +
                "WHERE username = ?")) {
            stmt.setString(1, username);
            try (ResultSet r = stmt.executeQuery()) {
                if (r.next()) {
                    return r.getString(1);
                } else {
                    return null;
                }
            }
        }
    }

    @Override
    public void close() throws Exception {
        this.conn.close();
    }

    public void createNewUser(User user) throws SQLException{
        try {

            PreparedStatement stmt = conn.prepareStatement("INSERT INTO blog_users VALUES (?,?,?,?,?,?,?,?)");
            {
                stmt.setString(1, user.getFname());
                stmt.setString(2, user.getLname());
                stmt.setString(3, user.getRole());
                stmt.setString(4, user.getUsername());
                stmt.setString(5, user.getBirthday());
                stmt.setString(6, user.getCountry());
                stmt.setString(7, user.getDescrp());
//                    stmt.setBlob(8, );
            }

        } catch (SQLException e) {
            e.getMessage();
        }


    }



    public void modifyUser(User user,String username) throws SQLException{

//        UPDATE table_name
//        SET column1 = value1, column2 = value2, ...
//        WHERE condition;

        try {

            PreparedStatement stmt = conn.prepareStatement("UPDATE blog_users SET Team_CBMM.blog_users.username=?," +
                    "password=?,Team_CBMM.blog_users.fname=?,Team_CBMM.blog_users.lname=?,Team_CBMM.blog_users.email=?," +
                    "Team_CBMM.blog_users.dob=?, Team_CBMM.blog_users.country=?,Team_CBMM.blog_users.descrp=?," +
                    "Team_CBMM.blog_users.avatar=?,Team_CBMM.blog_users.role=?,Team_CBMM.blog_users.country=? WHERE Team_CBMM.blog_users.username LIKE ?");
            {
                stmt.setString(1, user.getUsername());
                stmt.setString(2, user.getPassword());
                stmt.setString(3, user.getFname());
                stmt.setString(4, user.getLname());
                stmt.setString(5, user.getEmail());
                stmt.setString(6, user.getBirthday());
                stmt.setString(7, user.getCountry());
                stmt.setString(8, user.getDescrp());
                stmt.setString(9, user.getAvatar());
                stmt.setString(10, user.getRole());
                stmt.setString(11, user.getCountry());
                stmt.setString(12, username);
//                    stmt.setBlob(8, );
            }

        } catch (SQLException e) {
            e.getMessage();
        }


    }



}
