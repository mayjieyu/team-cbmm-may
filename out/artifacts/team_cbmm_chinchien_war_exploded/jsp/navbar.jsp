
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--todo: this JSP contains the navbar which needs to be included in some pages--%>
<%--the navbar should contain "main page", "user's page","account info", "log in", "login out"--%>
<%--the item we show should base on the user's role--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <%--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--%>
    <%--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>
    <%--<style>--%>
        <%--.navbar {--%>
            <%--margin-bottom: 0;--%>
            <%--border-radius: 0;}--%>
    <%--</style>--%>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="../image/i++.png" alt="" height="30px">
        </a>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
                <c:set var="role" scope="session" value="${user}"/>
                <c:choose>
                    <c:when test="${role == user}">
                        <%--<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"> me <span class="caret"></span></a>--%>
                            <%--<ul class="dropdown-menu">--%>
                                <%--<li><a href="#">My blog</a></li>--%>
                                <%--<li><a href="#">My account</a></li>--%>
                            <%--</ul>--%>
                        <%--</li>--%>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> My blog</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> My Account</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                    </c:when>
                    <c:otherwise>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                    </c:otherwise>
                </c:choose>
        </ul>
    </div>
</nav>


</body>
</html>

